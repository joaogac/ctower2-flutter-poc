import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/model/subcase_requirement.dart';

class DynamicWidget extends StatelessWidget {
  TextEditingController _textController = TextEditingController();
  TextEditingController _numericController = TextEditingController();

  String textValue() {
    return _textController.text;
  }

  int numericValue() {
    if (_numericController.text.trim().length > 0) {
      return int.parse(_numericController.text);
    }
    return -1;
  }

  Widget _createTextWidget() {
    return Container(
      width: 150,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: TextFormField(
        key: UniqueKey(),
        controller: _textController,
        decoration: InputDecoration(
            labelText: 'Text Field', border: OutlineInputBorder()),
      ),
    );
  }

  Widget _createNumericWidget() {
    return Container(
      width: 150,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: TextFormField(
          key: UniqueKey(),
          controller: _numericController,
          decoration: InputDecoration(labelText: "Enter your number"),
          keyboardType: TextInputType.number,
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly
          ] // Only numbers can be entered
          ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListBody(children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _createTextWidget(),
          _createNumericWidget(),
        ],
      ),
      /*Row(
        children: <Widget>[
           Container(
            child:  Radio(
              key:  UniqueKey(),
              value: 0,
              groupValue: null,
              onChanged: onChangedRadio,
            ),
          ),
           Container(
            child:  Text('Yes'),
          ),
           Container(
            child:  Radio(
              key:  UniqueKey(),
              value: 1,
              groupValue: null,
              onChanged: onChangedRadio,
            ),
          ),
           Container(
            child:  Text('No'),
          ),
          // https://api.flutter.dev/flutter/material/DropdownButton-class.html
           Container(
            padding: EdgeInsets.fromLTRB(10, 5, 5, 0),
            child:  DropdownButton<String>(
              key: UniqueKey(),
              value: "One",
              icon: Icon(Icons.arrow_downward),
              onChanged: onChangedDropDown,
              items: <String>['One', 'Two', 'Three', 'Four']
                  .map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
            ),
          )
        ],
      ),*/
    ]));
  }
}
