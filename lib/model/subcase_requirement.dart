/*
 * PODO - Plain Old Dart Object
 */
class SubCaseRequirement {
  String _name;
  String _type;
  dynamic _value;

  // named optional constructor parameter goes with curly braces {}
  SubCaseRequirement(this._name, this._type, {dynamic value}) : _value = value;
  // positional optional parameters goes with square brackets []
  // SubCaseRequirement(this._name, this._type, [this._value]);

  String get name => _name;
  String get type => _type;

  set value(dynamic value) => this._value = value;
  String get value => this._value;

  // DART factory constructor
  factory SubCaseRequirement.fromJson(Map<String, dynamic> jsonData) {
    return SubCaseRequirement(jsonData["name"], jsonData["type"]);
  }

  // See: https://stackoverflow.com/questions/49753412/converting-object-to-an-encodable-object-failed
  Map<String, dynamic> toJson() {
    return { 'name': _name, 'type': _type, 'value': _value };
  }
}
