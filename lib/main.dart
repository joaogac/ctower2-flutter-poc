import 'package:flutter/material.dart';
import 'package:myapp/model/subcase_requirement.dart';
import 'package:myapp/render/requirement_render.dart';
import 'package:myapp/utils/controltower_services.dart';

void main() {
  runApp(MyHomePage(title: 'Flutter Demo'));
}

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({this.title});

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // List<DynamicWidget> dynamicList = [];

  List<SubCaseRequirement> requirements = [];
  List<dynamic> _jsonData;

  @override
  void initState() {
    super.initState();
    _asyncInitState();
  }

  _asyncInitState() async {
    _jsonData = await ControlTowerServices().fetchJson();
  }

  loadRequirements() {
    requirements = [];
    for (var json in _jsonData) {
      final SubCaseRequirement subCaseRequirement =
          SubCaseRequirement.fromJson(json);
      requirements.add(subCaseRequirement);
    }
    setState(() {});
  }

  // addDynamic() {
  //   if (dynamicList.length >= 5) {
  //     dynamicList = [];
  //   } else {
  //     dynamicList.add(DynamicWidget());
  //   }
  //   // Refreshes the 'body' calling the build method bellow
  //   setState(() {});
  // }

  save() {
    // final List<dynamic> ret = [];
    // for (var i = 0; i < dynamicList.length; i++) {
    //   final dynamicWidget = dynamicList[i];

    //   final List<dynamic> array = [];
    //   final Map<String, dynamic> jsonText = {}, jsonNumeric = {};
    //   jsonText["name"] = "Texto";
    //   jsonText["type"] = "text";
    //   jsonText["value"] = dynamicWidget.textValue();
    //   array.add(jsonText);

    //   jsonNumeric["name"] = "Numerico";
    //   jsonNumeric["type"] = "number";
    //   jsonNumeric["value"] = dynamicWidget.numericValue();
    //   array.add(jsonNumeric);
    //   ret.add(array);
    // }

    ControlTowerServices().sendJson(requirements,
        callback: (int statusCode, String responseBody) {
      if (statusCode == 200) {
        _showNotification(" Success! ");
      } else {
        _showToast("HTTP Error '$statusCode': " + responseBody);
      }
    });
  }

  _showNotification(String message) {
    // showSimpleNotification( OverlaySupport( child: Text(message) ), background: Colors.cyan[300]);
    print(message);
  }

  _showToast(String message) {
    // toast(message);
    print(message);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Form(
                key: GlobalKey<FormState>(),
                child: ListView.builder(
                  itemCount: requirements.length, //dynamicList.length,
                  itemBuilder: (BuildContext context, int index) {
                    final SubCaseRequirement requirement = requirements[index];
                    var render = RequirementRender.fromRequirement(requirement);
                    return render.child;
                  },
                ),
              )),
          floatingActionButton: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 5),
                child: FloatingActionButton(
                  onPressed: save,
                  child: Icon(Icons.save),
                ),
              ),
              FloatingActionButton(
                onPressed: loadRequirements,
                child: Icon(Icons.add),
              ),
            ],
          ),
        ));
  }
}
