import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/model/subcase_requirement.dart';

class RequirementRender {
  Widget _child;
  // TextEditingController _controller;

  SubCaseRequirement _requirement;

  RequirementRender(this._child, /* this._controller, */ this._requirement)
      : assert(_child != null),
        // assert(_controller != null),
        assert(_requirement != null);

  Widget get child => _child;

  // dynamic get innerValue {
  //   switch (_requirement.type) {
  //     case "text":
  //       return _textValue();
  //     case "numeric":
  //       return _numericValue();
  //     case "date":
  //     default:
  //       return "";
  //   }
  // }

  // String _textValue() {
  //   return _controller.text;
  // }

  // int _numericValue() {
  //   if (_controller.text.trim().length > 0) {
  //     return int.parse(_controller.text);
  //   }
  //   return 0;
  // }

  // static updateSubCaseRequirementValue(
  //     RequirementRender render, SubCaseRequirement requirement) {
  //   requirement.value = render.innerValue();
  // }

  static Widget _createTextWidget(SubCaseRequirement requirement) {
    return Container(
      width: 150,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: TextFormField(
        key: UniqueKey(),
        // controller: controller,
        onChanged: (value) => requirement.value = value,
        decoration: InputDecoration(
            labelText: requirement.name, border: OutlineInputBorder()),
      ),
    );
  }

  static Widget _createNumericWidget(SubCaseRequirement requirement) {
    return Container(
      width: 150,
      padding: EdgeInsets.fromLTRB(5, 5, 5, 0),
      child: TextFormField(
          key: UniqueKey(),
          onChanged: (value) {
            if (value.trim().length > 0) {
              requirement.value = int.parse(value);
            } else {
              requirement.value = 0;
            }
          },
          decoration: InputDecoration(labelText: requirement.name),
          keyboardType: TextInputType.number,
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly
          ] // Only numbers can be entered
          ),
    );
  }

  static RequirementRender fromRequirement(SubCaseRequirement requirement) {
    // final TextEditingController controller = TextEditingController();

    switch (requirement.type) {
      case "text":
        return RequirementRender(_createTextWidget(requirement), requirement);
      case "numeric":
        return RequirementRender(
            _createNumericWidget(requirement), requirement);
      case "date":
      default:
        return RequirementRender(Text("TODO"), requirement);
    }

    // controller.addListener(updateSubCaseRequirementValue(render, requirement));
  }
}
