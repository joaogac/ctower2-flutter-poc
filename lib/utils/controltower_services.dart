import 'dart:convert';
import 'package:http/http.dart';

class ControlTowerServices {
  static const String _API_URL = "http://172.28.192.1:8080/api";
  static const String _TRANSACTION_DETAIL_URL = _API_URL + "/subcase";

  Future<List<dynamic>> fetchJson() async {
    final Response response = await get( _TRANSACTION_DETAIL_URL, headers: { 'Accept': 'application/json' } );
    return json.decode( response.body );
  }

  void sendJson(final List<dynamic> list, { Function(int, String) callback }) async {
    final String jsonStr = json.encode(list);

    final Response response = await post(_TRANSACTION_DETAIL_URL,
        headers: <String, String>{
          'Content-Type': 'application/json;charset=UTF-8',
        },
        body: jsonStr);

    callback(response.statusCode, response.body);
  }
}
