// Import the test package and Counter class
import 'dart:convert';

import 'package:myapp/model/subcase_requirement.dart';
import 'package:test/test.dart';

void main() {
  group('SubCaseRequirement JSON serialization', () {
    test('Step 1', () {
      final SubCaseRequirement requirement =
          SubCaseRequirement("Texto", "text");
      final String data = json.encode(requirement);
      expect(data, "{\"name\":\"Texto\",\"type\":\"text\",\"value\":null}");
    });

    test('Step 2', () {
      final List list = [];
      list.add(SubCaseRequirement("Texto", "text"));
      list.add(SubCaseRequirement("Numerico", "numeric"));
      final String data = json.encode(list);
      expect(data,
          "[{\"name\":\"Texto\",\"type\":\"text\",\"value\":null},{\"name\":\"Numerico\",\"type\":\"numeric\",\"value\":null}]");
    });
  });
}
